package priv.gyx.demo.pure;
/*
* @Description:
* @author GYx  
* @date 2018/8/20 10:41  
*/

import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class CodeDeserializer {

    private static final String CHARSET = "UTF-8";

    public static void main(String[] args) throws IOException {
        // 目标根目录路径
        String targetRootPath = "D:\\core\\private\\tst\\";
        // 代码文件路径
        String codeFilePath = "D:\\core\\private\\tst\\code.json";

        CodeDeserializer codeDeserializer = new CodeDeserializer();
        codeDeserializer.deserialize(targetRootPath, codeFilePath);
    }

    public void deserialize(String targetRootPath, String codeFilePath) throws IOException {
        // 读取代码文件
        String result = getFileContent(new File(codeFilePath));
        // 转换成json
        Map<String, String> contentMap = JSONObject.parseObject(result, Map.class);;
        // 逐个保存
        for(Map.Entry<String, String> entry : contentMap.entrySet()){
            String path = targetRootPath + entry.getKey();
            File file = new File(path);
            if(file.exists()){
                System.out.println(path + " 文件已存在");
                return;
            }
            if(!file.getParentFile().exists()){
                if(!file.getParentFile().mkdirs()){
                    System.out.println(path + " 所在目录创建失败");
                    return;
                }
            }
            try (FileWriter fileWriter = new FileWriter(file)){
                fileWriter.write(entry.getValue());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getFileContent(File file) throws IOException {
        try(FileInputStream in = new FileInputStream(file)){
            byte[] buf = new byte[(int)file.length()];
            in.read(buf);
            return new String(buf, CHARSET);
        } catch(Exception e){
            throw e;
        }
    }
}
